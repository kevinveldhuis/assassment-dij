@extends('layout')

@section('content')
    <div class="row row-cols-1">
        <h3>vernieuwen van deze pagina zorgt ervoor dat zowel het wachtwoord als de link niet langer zichtbaar zijn.</h3>
        <form action="{{ route('message.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <div class="mb-3">
                            <label for="share-link" class="form-label">Share link</label>
                            <input
                                type="text"
                                class="form-control"
                                id="share-link"
                                readonly
                                value="{{ \Illuminate\Support\Facades\URL::temporarySignedRoute(name: 'message.unlock.show', expiration: $message->valid_until, parameters:  $message->id)  }}"
                            >
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="text" class="form-control" id="password" readonly value="{{ $password }}">
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
