@extends('layout')

@section('content')
    <div class="row">

        <form action="{{ route('message.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <select name="colleague_id" class="form-control">
                            <option value="">Selecteer een collega</option>
                            @foreach (\App\Models\Colleague::get() as $colleague)
                                <option value="{{ $colleague->id }}">{{ $colleague->name }}</option>
                            @endforeach
                        </select>
                        @error('colleague_id')
                        <div class="text-danger">
                            {{ $message }}.
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                    <textarea required name="message" class="form-control" rows="5"
                              placeholder="Plaats hier je bericht*"></textarea>
                        @error('message')
                            <div class="text-danger">
                                {{ $message }}.
                            </div>
                        @enderror
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Versleutel bericht</button>
        </form>

    </div>
@endsection
