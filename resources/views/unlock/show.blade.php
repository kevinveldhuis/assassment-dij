@extends('layout')

@section('content')
    @if (session()->get('error'))
        <div class="alert alert-danger d-flex align-items-center" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
            <div>
                {{ session()->get('error') }}
            </div>
        </div>
    @endif
    <div class="row row-cols-1">
        <form action="{{ route('message.unlock.unlock', $message) }}" method="post">
            @csrf
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input name="password" type="password" class="form-control" id="password">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Ontgrendel bericht</button>
        </form>
    </div>
@endsection
