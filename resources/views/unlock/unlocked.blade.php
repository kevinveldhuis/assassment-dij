@extends('layout')

@section('content')
    <div class="row row-cols-1">
        <h3>Je kunt het bericht één keer lezen; daarna dien je opnieuw het wachtwoord in te voeren.</h3>
        <hr />

        <span class="font-weight-bold">Het verzonden bericht: </span>
        <p>{{ Crypt::decryptString($message->message) }}</p>
    </div>
    <a href="{{ URL::signedRoute('message.destroy', $message) }}" class="btn btn-danger">Verwijder bericht</a>

@endsection
