
# Installation

Requirements:
- Composer
- php 8.0 or higher
- some database server

```bash
cp .env.example .env
composer install
php artisan key:generate
```

Create a new database and update the database connection values in the `.env` file.
Then run:

```bash 
php artisan migrate
```

# Running the app

You can use your favourite stack (Docker, Mamp, Homestead). Or just run:

```bash
php artisan serve
```

# Run Tests

```bash
./vendor/bin/phpunit
```

# Import Colleagues
```bash
php artisan app:import-colleagues-from-pastebin
```

# Create message by api
There is a postman_collection added in the root of the project, if you import that one you can easily see how the API calls works from postman. For unlocking a message you need to pass the generated password as bearer key
