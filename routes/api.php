<?php

use App\Http\Controllers\Message\Api\CreateMessageController;
use App\Http\Controllers\UnlockMessage\Api\UnlockedMessageController;
use App\Http\Middleware\MessageApiMiddleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post(uri: 'message', action: CreateMessageController::class);
Route::middleware(MessageApiMiddleware::class)->group(function () {
    Route::get(uri: 'message/{message}', action: UnlockedMessageController::class)->name('message-unlocked');
});
