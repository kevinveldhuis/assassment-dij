<?php

use App\Http\Controllers\Message\CreateMessageController;
use App\Http\Controllers\Message\DestroyMessageController;
use App\Http\Controllers\Message\ShowMessageController;
use App\Http\Controllers\Message\StoreMessageController;
use App\Http\Controllers\UnlockMessage\ShowUnlockedMessageController;
use App\Http\Controllers\UnlockMessage\UnlockedMessageController;
use App\Http\Controllers\UnlockMessage\UnlockMessageController;
use Illuminate\Routing\Middleware\ValidateSignature;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', action: CreateMessageController::class)->name('message.create');

Route::prefix('message')->name('message.')->group(function () {
    Route::post(uri: '/store', action: StoreMessageController::class)->name(name: 'store');
    Route::get(uri: '/show/{message}', action: ShowMessageController::class)->name(name: 'show');
    Route::get(uri: '/delete/{message}', action: DestroyMessageController::class)->middleware(middleware: ValidateSignature::class)->name(name: 'destroy');

    Route::prefix('unlock')->name('unlock.')->group(function () {
        Route::get(uri: '/show/{message}', action: ShowUnlockedMessageController::class)->middleware(middleware: ValidateSignature::class)->name(name: 'show');
        Route::post(uri: '/unlock/{message}', action: UnlockMessageController::class)->name(name: 'unlock');
        Route::get(uri: '/unlocked/{message}', action: UnlockedMessageController::class)->name(name: 'unlocked');
    });
});

