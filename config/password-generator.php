<?php

return [
    'nouns' => ['Cat', 'Mountain', 'Ocean', 'Book', 'Sun', 'Tree', 'Car', 'Flower',
        'River', 'Bird', 'Laptop', 'Moon', 'Chair', 'House',
        'Music', 'Cloud', 'Dog', 'Beach', 'Star', 'Phone',
        'Coffee', 'Bicycle', 'Bridge', 'Island', 'Elephant',
        'Painting', 'Forest', 'Plane', 'Fish', 'Watch',
        'Camera', 'Butterfly', 'Castle', 'Rainbow', 'Garden', 'Hat',
        'Diamond', 'Clock', 'Shoes', 'Ball'
    ],
    'adjectives' => [
        'Beautiful', 'Majestic', 'Tranquil', 'Enchanting', 'Vibrant', 'Serene',
        'Mysterious', 'Cozy', 'Lively', 'Playful', 'Brilliant', 'Radiant',
        'Delicious', 'Quaint', 'Sparkling', 'Peaceful', 'Graceful', 'Exquisite',
        'Delightful', 'Energetic', 'Captivating', 'Colorful', 'Charming',
        'Glowing', 'Adorable', 'Stunning', 'Wonderful', 'Magnificent',
        'Blissful', 'Harmonious', 'Refreshing', 'Scenic', 'Joyful', 'Magical',
        'Splendid', 'Elegant', 'Playful', 'Enchanting', 'Serene', 'Tranquil',
    ]
];
