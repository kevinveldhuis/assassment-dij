<?php

namespace App\Providers;

use App\Services\PasswordGenerator\Contract\PasswordGeneratorContract;
use App\Services\PasswordGenerator\Lists\AdjectiveList;
use App\Services\PasswordGenerator\Lists\NounList;
use App\Services\PasswordGenerator\PasswordGenerator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(
            abstract: PasswordGeneratorContract::class,
            concrete: fn (): PasswordGeneratorContract => new PasswordGenerator(
                nouns: new NounList(
                    words: (array) config('password-generator.nouns'),
                ),
                adjectives: new AdjectiveList(
                    words: (array) config('password-generator.adjectives'),
                ),
            ),
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
