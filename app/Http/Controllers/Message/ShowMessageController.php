<?php

namespace App\Http\Controllers\Message;

use App\Exceptions\PasswordExpiredException;
use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Contracts\Support\Renderable;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ShowMessageController extends Controller
{
    /**
     * @param Message $message
     * @return Renderable
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(Message $message): Renderable
    {
        if (!session()->get(key: 'password')) {
            throw new PasswordExpiredException(message: 'Je password is verlopen');
        }

        return view('message.show')
            ->with([
                'password' => session()->get(key: 'password'),
                'message' =>  $message
            ]);
    }
}
