<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class CreateMessageController extends Controller
{
    /**
     * @return Renderable
     */
    public function __invoke(): Renderable
    {
        return view('message.create');
    }
}
