<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class DestroyMessageController extends Controller
{
    /**
     * @param Message $message
     * @return RedirectResponse
     */
    public function __invoke(Message $message): RedirectResponse
    {
        $message->delete();
        return redirect()->route(route: 'message.create');
    }
}
