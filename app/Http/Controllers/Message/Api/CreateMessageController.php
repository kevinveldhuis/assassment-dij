<?php

namespace App\Http\Controllers\Message\Api;

use App\Actions\Message\CreateNewMessageAction;
use App\DataTransferObjects\Message\MessageDataTransferObject;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\URL;

class CreateMessageController extends Controller
{
    public function __construct(private readonly CreateNewMessageAction $createNewMessageAction)
    { }

    /**
     * @param MessageDataTransferObject $request
     * @return JsonResponse
     */
    public function __invoke(MessageDataTransferObject $request): JsonResponse
    {
        $newMessage = $this->createNewMessageAction->run(dataTransferObject: $request);

        return new JsonResponse([
            'password' => $newMessage->password,
            'url' => route(name: 'api.message-unlocked', parameters: $newMessage->message)
        ]);
    }
}
