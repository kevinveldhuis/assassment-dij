<?php

namespace App\Http\Controllers\Message;

use App\Actions\Message\CreateNewMessageAction;
use App\DataTransferObjects\Message\MessageDataTransferObject;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class StoreMessageController extends Controller
{
    public function __construct(private readonly CreateNewMessageAction $createNewMessageAction)
    { }

    /**
     * @param MessageDataTransferObject $messageDataTransferObject
     * @return RedirectResponse
     */
    public function __invoke(MessageDataTransferObject $messageDataTransferObject): RedirectResponse
    {
        $createdMessage = $this->createNewMessageAction->run(dataTransferObject: $messageDataTransferObject);

        return redirect()
            ->route(route: 'message.show', parameters: $createdMessage->message->id)
            ->with(key: 'password', value: $createdMessage->password);
    }
}
