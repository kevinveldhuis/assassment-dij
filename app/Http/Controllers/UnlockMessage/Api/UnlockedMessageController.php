<?php

namespace App\Http\Controllers\UnlockMessage\Api;

use App\Models\Message;
use Illuminate\Http\JsonResponse;

class UnlockedMessageController
{
    /**
     * @param Message $message
     * @return JsonResponse
     */
    public function __invoke(Message $message): JsonResponse
    {
        return new JsonResponse([
            'secretMassage' =>  \Crypt::decryptString($message->message)
        ]);
    }
}
