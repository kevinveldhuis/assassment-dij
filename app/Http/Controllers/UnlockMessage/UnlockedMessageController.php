<?php

namespace App\Http\Controllers\UnlockMessage;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class UnlockedMessageController extends Controller
{
    /**
     * @param Message $message
     * @return View|RedirectResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(Message $message): View|RedirectResponse
    {
        if (!session()->get(key: 'unlocked')) {
            return redirect()->back();
        }

        return view(view: 'unlock.unlocked')->with(key: 'message', value: $message);
    }
}
