<?php

namespace App\Http\Controllers\UnlockMessage;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ShowUnlockedMessageController extends Controller
{
    /**
     * @param Message $message
     * @return View
     */
    public function __invoke(Message $message): View
    {
        return view(view: 'unlock.show')->with(key: 'message', value: $message);
    }
}
