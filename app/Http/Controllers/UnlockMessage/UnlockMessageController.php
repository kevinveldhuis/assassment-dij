<?php

namespace App\Http\Controllers\UnlockMessage;

use App\DataTransferObjects\Unlock\UnlockMessageDataTransferObject;
use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\RedirectResponse;

class UnlockMessageController extends Controller
{
    /**
     * @param Message $message
     * @param UnlockMessageDataTransferObject $unlockMessageDataTransferObject
     * @return RedirectResponse
     */
    public function __invoke(
        Message $message,
        UnlockMessageDataTransferObject $unlockMessageDataTransferObject
    ): RedirectResponse {
        $valid = \Hash::check(value: $unlockMessageDataTransferObject->password, hashedValue: $message->password);

        if ($valid) {
            return redirect()
                ->route(route: 'message.unlock.unlocked', parameters: $message)
                ->with(key: 'unlocked', value: true);
        }

        return redirect()->back()->with(key: 'error', value: 'password invalid');
    }
}
