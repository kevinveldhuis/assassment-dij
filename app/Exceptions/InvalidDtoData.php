<?php

namespace App\Exceptions;

use UnexpectedValueException;

class InvalidDtoData extends UnexpectedValueException
{ }
