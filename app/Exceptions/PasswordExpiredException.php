<?php

namespace App\Exceptions;

use Illuminate\Validation\UnauthorizedException;

class PasswordExpiredException extends UnauthorizedException
{

}
