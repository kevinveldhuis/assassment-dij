<?php

namespace App\DataTransferObjects\Colleague;

use App\Exceptions\InvalidDtoData;

class ColleagueDataTransferObject
{
    public function __construct(public readonly string $name, public readonly string $email)
    { }

    public static function create(array $colleague): ColleagueDataTransferObject
    {
        if (!isset($colleague['name']) || !isset($colleague['email'])) {
            throw new InvalidDtoData('Name or Email is not properly set');
        }

        return new self(name: $colleague['name'], email: $colleague['email']);
    }
}
