<?php

namespace App\DataTransferObjects\Unlock;

use _PHPStan_d55c4f2c2\Symfony\Contracts\Service\Attribute\Required;
use Spatie\LaravelData\Data;

class UnlockMessageDataTransferObject extends Data
{
    public function __construct(
        #[Required]
        public readonly string $password
    ) { }
}
