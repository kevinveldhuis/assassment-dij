<?php

namespace App\DataTransferObjects\Message;

use App\Models\Message;

class CreatedMessageDataTransferObject
{
    public function __construct(public readonly string $password, public readonly Message $message)
    {}
}
