<?php

namespace App\DataTransferObjects\Message;

use App\Models\Colleague;
use Spatie\LaravelData\Attributes\MapInputName;
use Spatie\LaravelData\Attributes\Validation\Exclude;
use Spatie\LaravelData\Attributes\Validation\Exists;
use Spatie\LaravelData\Attributes\Validation\IntegerType;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use Spatie\LaravelData\Optional;

#[MapInputName(SnakeCaseMapper::class)]
class MessageDataTransferObject extends Data
{
    public function __construct(
        public string $message,
        #[Exists(Colleague::class, 'id')]
        public ?int $colleagueId = null,
    ) { }
}
