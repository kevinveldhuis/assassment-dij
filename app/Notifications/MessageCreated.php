<?php

namespace App\Notifications;

use App\Models\Colleague;
use App\Models\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\URL;

class MessageCreated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(
        private readonly Colleague $colleague,
        private readonly Message   $message,
        private readonly string    $password
    )
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->line(
                sprintf(
                    'Hi %s, een collega probeert je een versleuteld bericht te versturen',
                    $this->colleague->name
                )
            )
            ->line('Je kan het bericht bekijken via de volgende knop')
            ->action('View message', URL::temporarySignedRoute(
                name: 'message.unlock.show',
                expiration: $this->message->valid_until,
                parameters: ['message' => $this->message->id]
            ))
            ->line(
                sprintf('En het vervolgens ontgrendelen met dit wachtwoord: %s', $this->password)
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
