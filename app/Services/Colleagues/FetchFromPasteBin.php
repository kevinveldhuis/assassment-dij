<?php

namespace App\Services\Colleagues;

use App\DataTransferObjects\Colleague\ColleagueDataTransferObject;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class FetchFromPasteBin
{
    /**
     * @param string $slug
     * @return Collection<array-key, ColleagueDataTransferObject>
     */
    public function getColleagues(string $slug): Collection
    {
        $response = Http::get(url: sprintf('https://pastebin.com/raw/%s', $slug));

        return $response->collect()->map(callback: function($colleague) {
            return ColleagueDataTransferObject::create($colleague);
        });
    }
}
