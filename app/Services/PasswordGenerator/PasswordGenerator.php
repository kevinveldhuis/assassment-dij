<?php

namespace App\Services\PasswordGenerator;

use App\Services\PasswordGenerator\Contract\PasswordGeneratorContract;
use App\Services\PasswordGenerator\Contract\ListContract;

final class PasswordGenerator implements PasswordGeneratorContract
{
    public function __construct(
        private readonly ListContract $nouns,
        private readonly ListContract $adjectives,
    ) {
    }

    public function generatePassword(): string
    {
        return $this->build(
            $this->nouns->generate(),
            $this->adjectives->generate(),
            $this->nouns->generate(),
            $this->adjectives->generate(),
        );
    }

    private function build(string ...$parts): string
    {
        return implode('-', $parts);
    }
}
