<?php

namespace App\Services\PasswordGenerator\Concerns;

trait HasWords
{
    public function __construct(private readonly array $words)
    { }


    public function generate(): string
    {
        $word = $this->words[array_rand($this->words)];

        $asArray = str_split(string: $word);

        $wordArray = array_map(
            callback: fn (string $item): string => $this->convertToNumerical($item),
            array: $asArray,
        );

        return implode(separator: '', array: $wordArray);
    }

    public function convertToNumerical(string $item): string
    {
        return match ($item) {
            'a' => '4',
            'e' => '3',
            'i' => '1',
            'o' => '0',
            default => $item
        };
    }
}
