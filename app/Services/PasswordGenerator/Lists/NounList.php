<?php

namespace App\Services\PasswordGenerator\Lists;

use App\Services\PasswordGenerator\Concerns\HasWords;
use App\Services\PasswordGenerator\Contract\ListContract;

class NounList implements ListContract
{
    use HasWords;
}
