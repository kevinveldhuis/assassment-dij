<?php

namespace App\Services\PasswordGenerator\Lists;

use App\Services\PasswordGenerator\Concerns\HasWords;
use App\Services\PasswordGenerator\Contract\ListContract;

class AdjectiveList implements ListContract
{
    use HasWords;
}
