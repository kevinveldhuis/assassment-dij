<?php

namespace App\Services\PasswordGenerator\Contract;

interface PasswordGeneratorContract
{
    public function generatePassword(): string;
}
