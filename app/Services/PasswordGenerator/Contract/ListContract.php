<?php

namespace App\Services\PasswordGenerator\Contract;

interface ListContract
{
    public function generate(): string;
}
