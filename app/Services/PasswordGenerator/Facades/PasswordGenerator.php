<?php

namespace App\Services\PasswordGenerator\Facades;

use App\Services\PasswordGenerator\Contract\PasswordGeneratorContract;
use Illuminate\Support\Facades\Facade;

/**
 * @method static string generatePassword()
 *
 * @see PasswordGeneratorContract
 */
class PasswordGenerator extends Facade
{
    /**
     * @return class-string
     */
    protected static function getFacadeAccessor(): string
    {
        return PasswordGeneratorContract::class;
    }
}
