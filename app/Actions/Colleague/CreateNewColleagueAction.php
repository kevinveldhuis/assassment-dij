<?php

namespace App\Actions\Colleague;

use App\DataTransferObjects\Colleague\ColleagueDataTransferObject;
use App\Models\Colleague;

class CreateNewColleagueAction
{
    public function run(ColleagueDataTransferObject $colleague): void
    {
        Colleague::updateOrCreate(
            attributes: ['name' => $colleague->name, 'email' => $colleague->email],
            values: ['name' => $colleague->name,'email' => $colleague->email]
        );
    }
}
