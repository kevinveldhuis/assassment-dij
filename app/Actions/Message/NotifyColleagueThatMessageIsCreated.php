<?php

namespace App\Actions\Message;

use App\Models\Colleague;
use App\Models\Message;
use App\Notifications\MessageCreated;

class NotifyColleagueThatMessageIsCreated
{
    public function run(Colleague $colleague, Message $message, string $password): void
    {
        $colleague->notify(new MessageCreated(colleague: $colleague, message: $message, password:  $password));
    }
}
