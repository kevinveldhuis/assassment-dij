<?php

namespace App\Actions\Message;

use App\DataTransferObjects\Message\CreatedMessageDataTransferObject;
use App\DataTransferObjects\Message\MessageDataTransferObject;
use App\Models\Colleague;
use App\Models\Message;
use App\Services\PasswordGenerator\Facades\PasswordGenerator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class CreateNewMessageAction
{
    public function __construct(private readonly NotifyColleagueThatMessageIsCreated $notifyColleagueThatMessageIsCreated)
    { }

    public function run(
        MessageDataTransferObject $dataTransferObject,
    ): CreatedMessageDataTransferObject  {
        $password = PasswordGenerator::generatePassword();

        $message = Message::create([
            'message' => \Crypt::encryptString($dataTransferObject->message),
            'password' => Hash::make($password),
            'valid_until' => Carbon::now()->addDay(),
        ]);

        if ($dataTransferObject->colleagueId) {
            $colleague = Colleague::findOrFail(id: $dataTransferObject->colleagueId);
            $message->colleague()->associate($colleague);
            $this->notifyColleagueThatMessageIsCreated->run(
                colleague: $colleague,
                message: $message,
                password: $password
            );
        }

        return new CreatedMessageDataTransferObject(password: $password, message: $message);
    }
}
