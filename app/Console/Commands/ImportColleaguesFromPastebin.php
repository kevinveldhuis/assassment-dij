<?php

namespace App\Console\Commands;

use App\Actions\Colleague\CreateNewColleagueAction;
use App\Services\Colleagues\FetchFromPasteBin;
use Illuminate\Console\Command;

class ImportColleaguesFromPastebin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-colleagues-from-pastebin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function __construct(private readonly CreateNewColleagueAction $createNewColleagueAction)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $fetcher = new FetchFromPasteBin();
        $colleagues = $fetcher->getColleagues(slug: 'uDzdKzGG');

        foreach ($colleagues as $colleague) {
            $this->createNewColleagueAction->run(colleague: $colleague);
        }
    }
}
