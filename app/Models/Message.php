<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Message extends Model
{
    protected $fillable = [
        'message',
        'password',
        'valid_until',
    ];

    protected $casts = [
        'valid_until' => 'datetime'
    ];

    /**
     * @return BelongsTo<Colleague, Message>
     */
    public function colleague(): BelongsTo
    {
        return $this->belongsTo(Colleague::class);
    }
}
