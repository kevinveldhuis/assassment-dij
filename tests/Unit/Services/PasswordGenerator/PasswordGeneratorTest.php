<?php

namespace Tests\Unit\Services\PasswordGenerator;

use App\Services\PasswordGenerator\Contract\ListContract;
use App\Services\PasswordGenerator\Lists\AdjectiveList;
use App\Services\PasswordGenerator\Lists\NounList;
use App\Services\PasswordGenerator\PasswordGenerator;
use PHPUnit\Framework\TestCase;

class PasswordGeneratorTest extends TestCase
{
    private array $words;

    public function setUp(): void
    {
        $this->words = ['test'];
    }

    public function test_can_instantiate_a_new_instance()
    {
        $passwordGenerator = new PasswordGenerator(
            nouns: new NounList(
                words: $this->words
            ),
            adjectives: new AdjectiveList(
                words: $this->words
            )
        );

        $this->assertInstanceOf(PasswordGenerator::class, $passwordGenerator);
    }

    public function test_can_create_a_noun_list()
    {
        $nounList = new NounList(words: $this->words);

        $this->assertInstanceOf(ListContract::class, $nounList);
    }

    public function test_can_generate_a_random_noun()
    {
        $nounList = new NounList(words: $this->words);

        $this->assertEquals('t3st', $nounList->generate());
    }

    public function test_can_create_a_adjective_list()
    {
        $nounList = new AdjectiveList(words: $this->words);

        $this->assertInstanceOf(ListContract::class, $nounList);
    }

    public function test_can_generate_a_adjective_noun()
    {
        $nounList = new AdjectiveList(words: $this->words);

        $this->assertEquals('t3st', $nounList->generate());
    }

    public function test_can_generate_a_password()
    {
        $passwordGenerator = new PasswordGenerator(
            nouns: new NounList(
                words: $this->words
            ),
            adjectives: new AdjectiveList(
                words: $this->words
            )
        );

        $password = $passwordGenerator->generatePassword();
        $this->assertIsString($password);
        $this->assertEquals('t3st-t3st-t3st-t3st', $password);
    }
}
