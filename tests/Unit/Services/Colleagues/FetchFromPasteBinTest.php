<?php

namespace Tests\Unit\Services\Colleagues;

use App\DataTransferObjects\Colleague\ColleagueDataTransferObject;
use App\Services\Colleagues\FetchFromPasteBin;
use Illuminate\Support\Facades\Http;
use PHPUnit\Framework\TestCase;

class FetchFromPasteBinTest extends TestCase
{
    protected function setUp(): void
    {
        Http::fake(callback: [
            'https://pastebin.com/raw/1234' => Http::response([
                [
                    'name' => 'test',
                    'email' => 'test@test.com',
                ]
            ])
        ]);

        Http::fake(callback: ['https://pastebin.com/raw/345' => Http::response([])]);
        Http::fake(callback: ['https://pastebin.com/raw/500' => Http::response([], 500)]);

    }

    public function test_can_instantiate_a_new_instance()
    {
        $fetchFromPasteBin = new FetchFromPasteBin();
        $this->assertInstanceOf(expected: FetchFromPasteBin::class, actual: $fetchFromPasteBin);
    }

    public function test_colleagues_are_returned_from_paste_bin()
    {
        $fetchFromPasteBin = new FetchFromPasteBin();
        $colleagues = $fetchFromPasteBin->getColleagues(slug: '1234');

        self::assertCount(expectedCount: 1, haystack: $colleagues);
    }

    public function test_empty_response_dont_fail()
    {
        $fetchFromPasteBin = new FetchFromPasteBin();
        $colleagues = $fetchFromPasteBin->getColleagues(slug: '');

        self::assertCount(expectedCount: 0, haystack: $colleagues);
    }

    public function test_collection_item_type_is_colleague_dto()
    {
        $fetchFromPasteBin = new FetchFromPasteBin();
        $colleagues = $fetchFromPasteBin->getColleagues(slug: '1234');

        $this->assertInstanceOf(expected: ColleagueDataTransferObject::class, actual: $colleagues->first());
    }
}
