<?php

namespace Tests\Unit\Actions\Colleague;

use App\Actions\Colleague\CreateNewColleagueAction;
use App\DataTransferObjects\Colleague\ColleagueDataTransferObject;
use App\Models\Colleague;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateNewColleagueActionTest extends TestCase
{
    use RefreshDatabase;

    private function getAction(): CreateNewColleagueAction
    {
        return new CreateNewColleagueAction();
    }

    public function test_colleague_is_created_when_dto_is_correct()
    {
        $colleagueTest = new ColleagueDataTransferObject(name: 'test', email: 'test@test.com');
        $action = $this->getAction();

        $action->run(colleague: $colleagueTest);
        $this->assertDatabaseCount(table: Colleague::class, count: 1);
    }

    public function test_colleague_is_updated_when_email_and_name_already_exists()
    {
        $colleagueTest = new ColleagueDataTransferObject(name: 'test', email: 'test@test.com');
        $action = $this->getAction();

        $action->run(colleague: $colleagueTest);

        $colleagueTest2 = new ColleagueDataTransferObject(name: 'test', email: 'test@test.com');
        $action->run(colleague: $colleagueTest2);

        $this->assertDatabaseCount(table: Colleague::class, count: 1);
    }
}
