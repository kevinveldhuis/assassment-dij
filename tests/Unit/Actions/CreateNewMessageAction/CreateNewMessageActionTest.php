<?php

namespace Tests\Unit\Actions\CreateNewMessageAction;

use App\Actions\Colleague\CreateNewColleagueAction;
use App\Actions\Message\CreateNewMessageAction;
use App\Actions\Message\NotifyColleagueThatMessageIsCreated;
use App\DataTransferObjects\Colleague\ColleagueDataTransferObject;
use App\DataTransferObjects\Message\CreatedMessageDataTransferObject;
use App\DataTransferObjects\Message\MessageDataTransferObject;
use App\Models\Colleague;
use App\Notifications\MessageCreated;
use Illuminate\Support\Facades\Notification;
use PHPUnit\Framework\TestCase;

class CreateNewMessageActionTest extends TestCase
{
    private MessageDataTransferObject $messageDataTransferObjectWithColleague;
    private MessageDataTransferObject $messageDataTransferObjectWithoutColleague;

    public function setUp(): void
    {
        parent::setUp();

        $this->messageDataTransferObjectWithColleague = new MessageDataTransferObject(message: 'test', colleagueId: 1);
        $this->messageDataTransferObjectWithoutColleague = new MessageDataTransferObject(message: 'test');
    }

    public function test_can_instantiate_a_new_instance()
    {
        $createNewMessageAction = $this->createMock(originalClassName: CreateNewMessageAction::class);
        $this->assertInstanceOf(expected: CreateNewMessageAction::class, actual: $createNewMessageAction);
    }

    public function test_can_create_a_new_message_without_a_colleague()
    {
        $notifyColleagueAction = $this->createMock(originalClassName: NotifyColleagueThatMessageIsCreated::class);
        app()->instance(NotifyColleagueThatMessageIsCreated::class, $notifyColleagueAction);

        $createNewMessageAction = $this
            ->getMockBuilder(className: CreateNewMessageAction::class)
            ->setConstructorArgs([$notifyColleagueAction])
            ->getMock();

        app()->instance(CreateNewMessageAction::class, $createNewMessageAction);

        $createdMessage = $createNewMessageAction->run($this->messageDataTransferObjectWithoutColleague);

        $this->assertInstanceOf(expected: CreatedMessageDataTransferObject::class, actual: $createdMessage);
    }

    public function test_can_create_a_new_message_with_a_colleague()
    {
        $notifyColleagueAction = $this->createMock(originalClassName: NotifyColleagueThatMessageIsCreated::class);
        app()->instance(NotifyColleagueThatMessageIsCreated::class, $notifyColleagueAction);

        $createNewMessageAction = $this
            ->getMockBuilder(className: CreateNewMessageAction::class)
            ->setConstructorArgs([$notifyColleagueAction])
            ->getMock();

        app()->instance(CreateNewMessageAction::class, $createNewMessageAction);

        $createdMessage = $createNewMessageAction->run(dataTransferObject: $this->messageDataTransferObjectWithColleague);

        $this->assertInstanceOf(expected: CreatedMessageDataTransferObject::class, actual: $createdMessage);
    }
}

